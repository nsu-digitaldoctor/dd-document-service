package com.digital.doctor.dddocumentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdDocumentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DdDocumentServiceApplication.class, args);
    }

}
